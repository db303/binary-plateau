## How to run it

Install rust language

```
$ curl https://sh.rustup.rs -sSf | sh
```


Build the application

```
$ cargo build
```

Run the program

```
./main
```

Run tests

```
$ cargo test
```
