use std::cmp;

pub fn solution(mut n: u32) -> u32 {

    let mut longest_count = 0;
    let mut current_count = 0;

    while n > 0 {
        
        if n & 1 == 1 {
            current_count = current_count + 1;
        } else {
            longest_count = cmp::max(current_count, longest_count);
            current_count = 0;
        }

        n = n >> 1;
    } 

    longest_count = cmp::max(current_count, longest_count);

    longest_count
}


#[test]
fn find_longest_plateau_of_0() {
    let result = solution(0);
    let expect = 0;

    assert_eq!(result, expect);
}

#[test]
fn find_longest_plateau_of_14() {
    let result = solution(14);
    let expect = 3;

    assert_eq!(result, expect);
}

#[test]
fn find_longest_plateau_of_758() {
    let result = solution(758);
    let expect = 4;

    assert_eq!(result, expect);
}

#[test]
fn find_longest_plateau_of_125() {
    let result = solution(125);
    let expect = 5;

    assert_eq!(result, expect);
}


