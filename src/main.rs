use std::io;
mod binary_plateau;

fn main() {
    let mut s = String::new();

    println!("Please enter a positive integer:");
    io::stdin().read_line(&mut s).unwrap();

    match s.trim_right().parse::<u32>() {
        Ok(i) => println!("The solution is {}", binary_plateau::solution(i)),
        Err(_) => println!("Invalid number."),
    }

}
